var fib = function (n) {
    if (n < 2) return n;
	return fib(n - 1) + fib(n - 2);
}

fib(5); // enter your N here